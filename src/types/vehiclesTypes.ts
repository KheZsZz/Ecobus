import { ObjectId } from "mongodb";

export type Vehicles = {
  _id?: ObjectId;
  placa: string;
  tipo: string;
  renavan: string;
  chassi: string;
  marca: string;
  modelo: string;
  cor: string;
  qtd_lugares: number;
  ano_fab: number;
  Documentos: DocumentsVehicles;
  data_aquisicao: Date;
  data_venda?: Date;
  corporate: string;
};

type DocumentsVehicles = {
  vcto: {
    artesp: Date;
    sptrans: Date;
    emtu: Date;
    licenciamento: Date;
    tacografo: Date;
    zona_rest: Date;
    antt?: Date;
    apolice: {
      vcto: Date;
      n_apolice: string;
    };
    extintores?: {
      tipo: string;
      vcto: Date;
      desc: string;
    };
  };
};
