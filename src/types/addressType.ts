export type Address = {
  cep: string;
  number: number;
  complement: string;
};
