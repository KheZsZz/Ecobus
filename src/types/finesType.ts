import { ObjectId } from "mongodb";

export type Fines = {
  _id?: ObjectId;
  driveId: ObjectId;
  placa: string;
  orgao_emissor: string;
  tipo: string;
  Aiimp: string;
  renainf: string;
  guia: string;
  infracao: string;
  data_infracao: Date;
  municipio: string;
  valor: number;
  desconto: number;
  data_venc: Date;
  titulo: number;
  desc: boolean;
};
