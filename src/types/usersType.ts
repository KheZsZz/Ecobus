import { ObjectId } from "mongodb";
import { Address } from "./addressType";

type Documents = {
  cpf: string;
  rg: string;
  pis: string | null;
  cnh: CNH;
};

type CNH = {
  vcto: Date;
  number: string;
};

type BankData = {
  agency: number;
  account: number;
  digit: number;
  bank: string;
  pix: string;
};

export interface Users {
  _id?: ObjectId;
  first_name: string;
  last_name: string;
  address: Address;
  documents: Documents;
  bankData: BankData;
  ofice: string;
  phone: string;
  email: string;
  password: string;
  corporate: ObjectId;
}
export interface Employed extends Users {}
