import { ObjectId } from "mongodb";
import { Address } from "./addressType";

export interface Corporate {
  _id?: ObjectId;
  corporate: string;
  cnpj: string;
  IE?: string; //inscrição estadual
  IM?: string; // inscrição municipal
  responsable: string;
  phone: string;
  email?: string;
  address: Address;
}
