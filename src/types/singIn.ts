import { ObjectId } from "mongodb";

export interface SingInType {
  _id?: string;
  email: string;
  password: string;
}
