import Router from "next/router";
import { setCookie } from "nookies";
import type { SingInType } from "../types/singIn";
import { apiRequest } from "./axios";

export const SingIn = async ({ email, password }: SingInType) => {
  const api = await apiRequest();
  const isAuthenticated = await api.post("/users/singIn", {
    email,
    password,
  });

  switch (isAuthenticated.status) {
    case 200:
      setCookie(undefined, "ecobus_token", isAuthenticated.data.token, {
        maxAge: 60 * 60 * 1, //1 hora
      });
      Router.push("/");
      break;
    case 404:
      Router.push("/register"); //registers
      break;
    case 401:
      alert(isAuthenticated.data.message);
      break;
    default:
      alert({ message: `Email or password invalid` });
  }
};
