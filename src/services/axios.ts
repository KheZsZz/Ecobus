import axios from "axios";
import { parseCookies } from "nookies";

export const apiRequest = async (ctx?: any) => {
  const { ecobus_token: token } = parseCookies(ctx);

  const api = axios.create({
    baseURL: `http://localhost:3000/api`,
    headers: {
      "Content-Type": "application/json",
    },
    timeout: 5000,
  });
  if (token) {
    api.defaults.headers.authorization = `Bearer ${token}`;
    return api;
  } else {
    return api;
  }
};
