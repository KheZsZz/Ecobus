import { Db, MongoClient } from "mongodb";

type ConnectType = {
  database: Db;
  client: MongoClient;
  error?: unknown;
};

const client = new MongoClient(String(process.env.MONGODB_URI));
const database = client.db(String(process.env.MONGODB_DATABASE));

export const dbConnect = async (): Promise<ConnectType> => {
  try {
    await client.connect();
    console.log("BD Conectado!");
    return {
      database,
      client,
    };
  } catch (error) {
    console.log(error);
    return { client, database };
  }
};

export const UsersColletion = database.collection("users");
export const CorporateColletion = database.collection("corporate");
export const VehiclesColletion = database.collection("vehicles");
export const FinesColletion = database.collection("fines");
