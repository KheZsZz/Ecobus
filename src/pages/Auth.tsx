import { createContext } from "react";
import { SingIn } from "../services/auth";
import type { SingInType } from "../types/singIn";

type AuthContextType = {
  SingIn: (data: SingInType) => Promise<void>;
};

export const AuthContext = createContext({} as AuthContextType);

export const Auth = ({ children }: any) => {
  return (
    <AuthContext.Provider value={{ SingIn }}>{children}</AuthContext.Provider>
  );
};
