import { NextPage } from "next";
import NavBar from "../components/navbar";

const trips: NextPage = () => {
  return (
    <div>
      <NavBar />
      <h1>Trips</h1>
    </div>
  );
};
export default trips;
