import { NextPage } from "next";
import { useContext } from "react";
import { useForm } from "react-hook-form";
import { AuthContext } from "./Auth";
import type { SingInType } from "../types/singIn";

const LogIn: NextPage = () => {
  const { register, handleSubmit } = useForm<SingInType>();
  const { SingIn } = useContext(AuthContext);

  const HandleSingIn = async ({ email, password }: SingInType) => {
    await SingIn({ email: email, password: password });
  };

  return (
    <div>
      <form onSubmit={handleSubmit(HandleSingIn)}>
        <input
          {...register("email")}
          placeholder="E-mail:"
          type="email"
          id="email"
        />
        <br />
        <input
          {...register("password")}
          placeholder="Password:"
          type="password"
          id="password"
        />
        <br />
        <input type="submit" value="SingIn" />
        <br />
      </form>
    </div>
  );
};
export default LogIn;
