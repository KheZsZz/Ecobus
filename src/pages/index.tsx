import type {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import type { Users } from "../types/usersType";
import { apiRequest } from "../services/axios";
import { parseCookies } from "nookies";
import { decode } from "jsonwebtoken";
import { Corporate } from "../types/corporateType";
import NavBar from "../components/navbar";

const Home: NextPage = (
  props: InferGetServerSidePropsType<typeof getServerSideProps | Users>
) => {
  // console.log(props);
  return (
    <div>
      <NavBar />
      <h1>Dashboard</h1>
      <h1>Bem vindo {props.user.first_name} !!!</h1>
      <h1>{props.corporate.corporate}</h1>
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { ecobus_token: token } = parseCookies(ctx);
  const api = await apiRequest(ctx);
  if (!token) {
    return {
      redirect: {
        permanent: false,
        destination: "/login",
      },
    };
  } else {
    const userId = decode(token);

    const { data: user } = await api.patch<Users>(`/users/${userId}`);
    const { data: corporate } = await api.patch<Corporate>(
      `/corporate/${user.corporate}`
    );

    return {
      props: {
        user,
        corporate,
      },
    };
  }
};

export default Home;
