import { decode } from "jsonwebtoken";
import {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import { parseCookies } from "nookies";
import NavBar from "../components/navbar";
import { apiRequest } from "../services/axios";
import { Fines } from "../types/finesType";
import Styles from "../styles/fines.module.css";

const fines: NextPage = (
  props: InferGetServerSidePropsType<typeof getServerSideProps>
) => {
  return (
    <div>
      <NavBar />
      <h1>Fines</h1>
      <h2>Total de multas: {props.fines.length}</h2>
      {props.fines?.map((item: Fines) => (
        <div key={String(item._id)} className={Styles.finesContainer}>
          <h4>{item.Aiimp}</h4>
          <h4>{item.placa}</h4>
          <h4>{item.orgao_emissor}</h4>
          <p>{item.infracao}</p>
          <h4>valor: R${item.valor}</h4>
        </div>
      ))}
    </div>
  );
};

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { ecobus_token: token } = parseCookies(ctx);

  if (!token) {
    return {
      redirect: {
        permanent: false,
        destination: "/login",
      },
    };
  } else {
    const api = await apiRequest(ctx);
    const userId = decode(token);
    const { data: fines } = await api.get<Fines[]>(`/vehicles/fines/${userId}`);

    return {
      props: { fines },
    };
  }
};
export default fines;
