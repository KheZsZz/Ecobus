import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, CorporateColletion } from "../../../config/database";
import type { Corporate } from "../../../types/corporateType";

const handleCorporate = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  await dbConnect();
  const corporate: Corporate = req.body;

  switch (req.method) {
    case "GET":
      try {
        const corporates = await CorporateColletion.find<Corporate>(
          {}
        ).toArray();
        res.status(200).json(corporates);
      } catch (error) {
        res.status(500).json({ error });
      }
      break;

    case "POST":
      try {
        const getOne = await CorporateColletion.findOne({
          cnpj: corporate.cnpj,
        });
        if (!getOne) {
          const insertCorporate = await CorporateColletion.insertOne(corporate);
          res.status(201).json(insertCorporate);
        } else {
          res.status(301).json({ message: `Corporção já cadastrada.` });
        }
      } catch (error) {
        res.status(500).json({ message: `Erro ao cadastrar corporação` });
      }
      break;

    default:
      res.status(400).json({ message: `Error` });
  }
};
export default handleCorporate;
