import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, CorporateColletion } from "../../../config/database";
import { Corporate } from "../../../types/corporateType";
import { ObjectId } from "mongodb";

const handleCorporateID = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  await dbConnect();

  const { _id } = req.query;
  const newCorporate: Corporate = req.body;

  const { authorization } = req.headers;

  if (authorization) {
    switch (req.method) {
      case "PATCH":
        try {
          const corporate = await CorporateColletion.findOne<Corporate>({
            _id: new ObjectId(String(_id)),
          });
          res.status(200).json(corporate);
        } catch (error) {
          res.status(500).json({ error });
        }
        break;

      case "PUT":
        try {
          await CorporateColletion.findOneAndUpdate(
            { _id: new ObjectId(String(_id)) },
            {
              $set: {
                corporate: newCorporate.corporate,
                cnpj: newCorporate.cnpj,
                IE: newCorporate.IE,
                IM: newCorporate.IM,
                responsable: newCorporate.responsable,
                phone: newCorporate.phone,
                email: newCorporate.email,
                address: {
                  cep: newCorporate.address.cep,
                  number: newCorporate.address.number,
                  complement: newCorporate.address.complement,
                },
              },
            },
            { upsert: true, raw: true }
          );

          res.status(200).json({
            message: `Corporate alterado com sucesso`,
          });
        } catch (error: any) {
          res.status(404).json({
            message: `Corporação não encontrada...`,
            error: error.message,
          });
        }
        break;

      case "DELETE":
        try {
          const dell = await CorporateColletion.findOneAndDelete({
            _id: new ObjectId(String(_id)),
          });
          res.status(200).json(dell);
        } catch (error) {
          res.status(404).json({ message: `Corporate not exist` });
        }
        break;

      default:
        res.status(400).json({ message: `bad request` });
    }
  } else {
    res
      .status(401)
      .json({ message: `Usuario não autenticado...`, redirect: `/` });
  }
};
export default handleCorporateID;
