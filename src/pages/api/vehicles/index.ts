import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, VehiclesColletion } from "../../../config/database";
import { Vehicles } from "../../../types/vehiclesTypes";

type typeError = {
  message?: string;
  inserdId?: string;
};

const handleVehicles = async (
  req: NextApiRequest,
  res: NextApiResponse<Vehicles[] | Vehicles | typeError>
): Promise<void> => {
  await dbConnect();
  const vehicle: Vehicles = req.body;

  switch (req.method) {
    case "GET":
      try {
        const vehicles = await VehiclesColletion.find<Vehicles>({}).toArray();
        res.status(200).json(vehicles);
      } catch (error) {
        res.status(404).json({ message: `Erro na requisição GET all` });
      }
      break;

    case "POST":
      try {
        const getOne = await VehiclesColletion.findOne<Vehicles>({
          placa: vehicle.placa,
        });

        if (!getOne) {
          const insertVehicle = await VehiclesColletion.insertOne(vehicle);
          res.status(201).json({ inserdId: String(insertVehicle.insertedId) });
        } else {
          res.status(301).json({ message: `Veiculo já cadastrado!` });
        }
      } catch (error) {
        res.status(500).json({ message: `Erro ao cadastrar veiculo` });
      }
      break;

    default:
      res.status(400).json({ message: `Error in request ${req.query}` });
  }
};
export default handleVehicles;
