import { ObjectId } from "mongodb";
import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, VehiclesColletion } from "../../../config/database";
import { Vehicles } from "../../../types/vehiclesTypes";

const handleVehicleID = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  await dbConnect();

  const { _id } = req.query;
  const newVehicle: Vehicles = req.body;

  switch (req.method) {
    case "PATCH":
      try {
        const vehicle = await VehiclesColletion.findOne<Vehicles>({
          _id: new ObjectId(String(_id)),
        });
        res.status(200).json(vehicle);
      } catch (error) {
        res.status(500).json({ error });
      }
      break;

    case "PUT":
      try {
        await VehiclesColletion.findOneAndUpdate(
          { _id: new ObjectId(String(_id)) },
          {
            $set: {
              placa: newVehicle.placa,
              marca: newVehicle.marca,
              chassi: newVehicle.chassi,
              modelo: newVehicle.modelo,
              cor: newVehicle.cor,
              corporate: newVehicle.corporate,
              Documentos: {
                vcto: {
                  artesp: newVehicle.Documentos.vcto.artesp,
                  sptrans: newVehicle.Documentos.vcto.sptrans,
                  emtu: newVehicle.Documentos.vcto.emtu,
                  licenciamento: newVehicle.Documentos.vcto.licenciamento,
                  tacografo: newVehicle.Documentos.vcto.tacografo,
                  zona_rest: newVehicle.Documentos.vcto.zona_rest,
                  antt: newVehicle.Documentos.vcto.antt,
                  apolice: {
                    vcto: newVehicle.Documentos.vcto.apolice.vcto,
                    n_apolice: newVehicle.Documentos.vcto.apolice.n_apolice,
                  },
                  extintores: {
                    tipo: newVehicle.Documentos.vcto.extintores?.tipo,
                    vcto: newVehicle.Documentos.vcto.extintores?.vcto,
                    desc: newVehicle.Documentos.vcto.extintores?.desc,
                  },
                },
              },
              data_aquisicao: newVehicle.data_aquisicao,
              data_venda: newVehicle.data_venda,
            },
          },
          { upsert: true, raw: true }
        );
        res.status(200).json({
          message: `Veiculo alterado com sucesso`,
        });
      } catch (error: any) {
        res.status(404).json({
          message: `Veiculo não encontrada...`,
          error: error.message,
        });
      }
      break;

    case "DELETE":
      try {
        const dell = await VehiclesColletion.deleteOne({
          _id: new ObjectId(String(_id)),
        });
        res.status(200).json(dell);
      } catch (error) {
        res.status(404).json({ message: `Veiculo não existe...` });
      }
      break;

    default:
      res.status(400).json({ message: `bad request` });
  }
};
export default handleVehicleID;
