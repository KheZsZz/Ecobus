import { ObjectId } from "mongodb";
import { NextApiRequest, NextApiResponse } from "next";
import { FinesColletion, dbConnect } from "../../../../config/database";
import { Fines } from "../../../../types/finesType";

const handleFines = async (req: NextApiRequest, res: NextApiResponse) => {
  await dbConnect();

  const { authorization } = req.headers;
  if (authorization) {
    const { params = [] } = req.query;
    const fine: Fines = req.body;

    switch (req.method) {
      case "GET":
        const allfines = await FinesColletion.find<Fines>({
          driveId: params[0],
          desc: false,
        }).toArray();
        res.status(200).json(allfines);
        break;

      case "PATCH":
        const allfinesDrives = await FinesColletion.find<Fines>({
          placa: params[0],
          driveId: new ObjectId(String(params[1])),
        }).toArray();
        res.status(200).json(allfinesDrives);
        break;

      case "POST":
        const getOne = await FinesColletion.findOne({
          placa: fine.placa,
          renainf: fine.renainf,
          Aiimp: fine.Aiimp,
          guia: fine.guia,
        });
        if (!getOne) {
          const insertFine = await FinesColletion.insertOne(fine);
          res.status(201).json(insertFine);
        } else {
          res.status(404).json({ message: `Multa já cadastrada!` });
        }
        break;

      case "DELETE":
        const dell = await FinesColletion.findOneAndDelete({
          _id: new ObjectId(String(params[1])),
        });
        res.status(200).json(dell);
        break;

      case "PUT":
        await FinesColletion.findOneAndUpdate(
          {
            _id: new ObjectId(String(params[0])),
          },
          {
            $set: fine,
          },
          { raw: true, upsert: true }
        );
        res.status(200).json({ message: `Multa alterada comsucesso!` });
        break;

      default:
        res.status(400).json({ message: `bad request` });
    }
  } else {
    res
      .status(401)
      .json({ message: `Usuario não autenticado...`, redirect: `/` });
  }
};
export default handleFines;
