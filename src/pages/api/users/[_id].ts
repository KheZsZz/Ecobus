import { ObjectId } from "mongodb";
import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, UsersColletion } from "../../../config/database";
import { Users } from "../../../types/usersType";

const handleUserID = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  await dbConnect();

  const { _id } = req.query;
  const newUser: Users = req.body;

  switch (req.method) {
    case "PATCH":
      try {
        const user = await UsersColletion.findOne({
          _id: new ObjectId(String(_id)),
        });
        res.status(200).json(user);
      } catch (error) {
        res.status(500).json({ error });
      }
      break;

    case "PUT":
      try {
        const alterUser = await UsersColletion.findOneAndUpdate(
          { _id: new ObjectId(String(_id)) },
          {
            $set: {
              first_name: newUser.first_name,
              last_name: newUser.last_name,
              ofice: newUser.ofice,
              phone: newUser.phone,
              email: newUser.email,
              password: newUser.password,
              corporate: newUser.corporate,
              address: {
                cep: newUser.address.cep,
                number: newUser.address.number,
                complement: newUser.address.complement,
              },
              documents: {
                cpf: newUser.documents.cpf,
                rg: newUser.documents.rg,
                pis: newUser.documents.pis,
                cnh: {
                  vcto: newUser.documents.cnh.vcto,
                  number: newUser.documents.cnh.number,
                },
              },
              bankData: {
                agency: newUser.bankData.agency,
                account: newUser.bankData.account,
                digit: newUser.bankData.digit,
                bank: newUser.bankData.bank,
                pix: newUser.bankData.pix,
              },
            },
          },
          { upsert: true, raw: true }
        );

        res.status(200).json({
          message: `User alterado com sucesso`,
        });
      } catch (error: any) {
        res.status(404).json({
          message: `User não encontrado`,
          userID: _id,
          error: error.message,
        });
      }
      break;

    case "DELETE":
      try {
        const dell = await UsersColletion.deleteOne({
          _id: new ObjectId(String(_id)),
        });
        res.status(200).json(dell);
      } catch (error) {
        res.status(404).json({ message: `User not exist` });
      }
      break;

    default:
      res.status(400).json({ message: `Error` });
  }
};
export default handleUserID;
