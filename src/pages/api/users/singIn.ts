import { sign } from "jsonwebtoken";
import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, UsersColletion } from "../../../config/database";
import type { SingInType } from "../../../types/singIn";
import { Users } from "../../../types/usersType";

const handleSingIn = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  await dbConnect();
  const { email, password }: SingInType = req.body;

  switch (req.method) {
    case "POST":
      try {
        if (email == null || password == null) {
          res.status(401).json({ message: `Valores nulos` });
        }
        const data = await UsersColletion.findOne<Users>({
          email: email,
          password: password,
        });

        data
          ? res.status(200).json({
              isAuth: true,
              token: sign(String(data._id), String(process.env.SECRET_TOKEN)),
            })
          : res.status(401).json({
              isAuth: false,
              message: `Email or password invalid`,
            });
      } catch (error) {
        res.status(406).json({
          message: `Error in request, error: ${error}`,
        });
      }
    default:
      res.status(401).json({ message: `unauthorized` });
  }
};
export default handleSingIn;
