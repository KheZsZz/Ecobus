import { NextApiRequest, NextApiResponse } from "next";
import { dbConnect, UsersColletion } from "../../../config/database";
import type { Users } from "../../../types/usersType";

const handleUsers = async (
  req: NextApiRequest,
  res: NextApiResponse
): Promise<void> => {
  await dbConnect();
  const users: Users = req.body;

  switch (req.method) {
    case "GET":
      try {
        const users = await UsersColletion.find<Users>({}).toArray();
        res.status(200).json(users);
      } catch (error) {
        res.status(500).json({ error });
      }
      break;

    case "POST":
      try {
        const getOne = await UsersColletion.findOne({
          email: users.email,
          documents: { cpf: users.documents.cpf },
        });
        if (!getOne) {
          const data = await UsersColletion.insertOne(users);
          res.status(201).json(data);
        } else {
          res.status(404).json({ message: `usuário já cadastrado...` });
        }
      } catch (error) {
        res.status(500).json({ error: `Error ao cadastrar o usuário` });
      }
      break;

    default:
      res.status(400).json({ message: `Error` });
  }
};
export default handleUsers;
