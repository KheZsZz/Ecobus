import Link from "next/link";
import Router from "next/router";

import { destroyCookie } from "nookies";

const NavBar = (ctx: any) => {
  const quit = async () => {
    destroyCookie(ctx, "ecobus_token");
    Router.reload();
  };

  return (
    <nav>
      <ul>
        <li>
          <Link href="/">
            <a>Dashboard</a>
          </Link>
        </li>
        <li>
          <Link href="fines">
            <a>Fines</a>
          </Link>
        </li>
        <li>
          <Link href="services">
            <a>Services</a>
          </Link>
        </li>
        <li>
          <Link href="trips">
            <a>Trips</a>
          </Link>
        </li>
        <li>
          <Link href="fuel">
            <a>Fuel</a>
          </Link>
        </li>
        <li>
          <a onClick={quit}>Quit</a>
        </li>
      </ul>
    </nav>
  );
};
export default NavBar;
